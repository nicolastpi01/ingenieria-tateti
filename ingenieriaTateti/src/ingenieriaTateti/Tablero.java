package ingenieriaTateti;

public class Tablero {
	private String[][] tablero;
	
	public Tablero() {
		tablero = new String[3][3];
	}
	
	public void ponerx(int coordx, int coordy) {
		tablero[coordx][coordy] = "x";
	}
	
	public String get(int coordx, int coordy) {
		return tablero[coordx][coordy];
	}

}
